require "shapes"

function love.load()
    lastDirection = "up"
    bullets = {}
    addNonPlayerRectangle(200, 50, 200, 150)
    playerRect = Rectangle(100, 50, 50, 50)
    move = true
    moveamount = 200

    keyboard_keys_functions = {}
    function keyboard_keys_functions.right(dt) playerRect:move("x", moveamount * dt) lastDirection = "right" end
    function keyboard_keys_functions.left(dt) playerRect:move("x", -moveamount * dt) lastDirection = "left" end
    function keyboard_keys_functions.up(dt) playerRect:move("y", -moveamount * dt) lastDirection = "up" end
    function keyboard_keys_functions.down(dt) playerRect:move("y", moveamount * dt) lastDirection = "down" end
--  keyboard_keys_functions = {}
--  keyboard_keys_functions["right"] = function(dt) playerRect.x = playerRect.x + moveamount * dt end
--  keyboard_keys_functions["left"] = function(dt) playerRect.x = playerRect.x - moveamount * dt end
--  keyboard_keys_functions["up"] = function(dt) playerRect.y = playerRect.y - moveamount * dt end
--  keyboard_keys_functions["down"] = function(dt) playerRect.y = playerRect.y + moveamount * dt end

end

function love.keypressed(key)
    if key == "space" then
        addRandomRectangle()
    elseif key == "escape" then
        love.event.quit()
    elseif key == "f" then
        x = playerRect.x + playerRect.width/2
        y = playerRect.y + playerRect.height/2
        newBullet = Rectangle(x, y, 10, 10)
        newBullet.direction = lastDirection
        table.insert(bullets, newBullet)
    end
end

function love.update(dt)
    for key, key_function in pairs(keyboard_keys_functions) do
        if love.keyboard.isDown(key) then
            key_function(dt)
        end
    end

    for i=1,#bullets do
        bullet = bullets[i]
        if bullet.x <= 20 or bullet.x >= love.graphics.getWidth() - 30
        or bullet.y <= 20 or bullet.y >= love.graphics.getHeight() - 30 then
            table.remove(bullets, i)
            return
        end
        if bullet.direction == "right" then
            bullet:move("x", moveamount * dt)
        elseif bullet.direction == "left" then
            bullet:move("x", -moveamount * dt)
        elseif bullet.direction == "up" then
            bullet:move("y", -moveamount * dt)
        elseif bullet.direction == "down" then
            bullet:move("y", moveamount * dt)
        end
    end
end


function love.draw()
    love.graphics.setColor(1,1,1,1)
    playerRect:draw()
    love.graphics.line({500,500,405,405})
    love.mouse.setVisible(false)
    mx, my = love.mouse.getPosition()
    love.graphics.circle("line", mx, my, 5)
    for i=1,#non_player_rectangles do
        local r = non_player_rectangles[i]
        if playerRect:checkCollision(r) then
            --If there is collision, draw the rectangles red
            love.graphics.setColor(1,0,0,1)
            r:draw("line")
        else
            --else, draw the rectangles white
            love.graphics.setColor(1,1,1,1)
            r:draw("line")
        end
    end
    for i=1,#bullets do
        bullets[i]:draw()
    end
end
