Object = require "libs.classic"

Rectangle = Object:extend()

function Rectangle:new(x, y, width, height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
end

function Rectangle.move(self, axis, amount)
    self[axis] = self[axis] + amount
end

function Rectangle.draw(self, mode)
    mode = mode or "line"
    love.graphics.rectangle(mode, self.x, self.y, self.width, self.height)
end

function Rectangle.checkCollision(a, b)
    --With locals it's common usage to use underscores instead of camelCasing
    local a_left = a.x
    local a_right = a.x + a.width
    local a_top = a.y
    local a_bottom = a.y + a.height

    local b_left = b.x
    local b_right = b.x + b.width
    local b_top = b.y
    local b_bottom = b.y + b.height

    --If Red's right side is further to the right than Blue's left side.
    if a_right > b_left and
    --and Red's left side is further to the left than Blue's right side.
    a_left < b_right and
    --and Red's bottom side is further to the bottom than Blue's top side.
    a_bottom > b_top and
    --and Red's top side is further to the top than Blue's bottom side then..
    a_top < b_bottom then
        --There is collision!
        return true
    else
        --If one of these statements is false, return false.
        return false
    end
end

function addNonPlayerRectangle(x, y, width, height)
    if non_player_rectangles == nil then
        non_player_rectangles = {}
    end
    newRect = Rectangle(x, y, width, height)
    table.insert(non_player_rectangles, newRect)
end

function addRandomRectangle()
    local x = love.math.random(100, 500)
    local y = love.math.random(100, 500)
    local width = love.math.random(100, 500)
    local height = love.math.random(100, 500)
    addNonPlayerRectangle(x, y, width, height)
end

Projectile = Rectangle:extend()

function Projectile:new(x, y, width, height)
    Projectile.super.new(self, x, y, width, height)
    self.direction = direction or "up"
end
