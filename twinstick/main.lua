-------------Commands for the main LOVE functions------------

local function create_canvas(size_x, size_y)
   local canvas = love.graphics.newCanvas(size_x, size_y)
   love.graphics.setCanvas(canvas)
   love.graphics.clear()
   love.graphics.setBlendMode("alpha")
--   love.graphics.setColor(1, 0, 0, 0.5)
   love.graphics.rectangle('fill', 0, 0, 100, 100)
   love.graphics.setCanvas()

   return canvas
end

local function draw_border_on_canvas(canvas)
   love.graphics.setCanvas(canvas)
   love.graphics.polygon("line",
                         0, 0,
                         canvas:getWidth(), 0,
                         canvas:getWidth(), canvas:getHeight(),
                         0, canvas:getHeight())

   love.graphics.setCanvas()

end

local function draw_canvas_to_root_window(canvas, pos_x, pos_y, canvas_scale_x)
   love.graphics.draw(canvas, pos_x, pos_y, 0, canvas_scale_x)
end

function update_canvas_position()
   if canvas_pos_x == nil then canvas_pos_x = 0 end
   if canvas_scale_x == nil then canvas_scale_x = 1 end
   if scaling == nil then scaling = "smaller" end

   if movement == "right" or movement == nil then
      canvas_pos_x = canvas_pos_x + 1
      if canvas_pos_x >= love.graphics.getWidth() - 400 then
         movement = "left"
      end
   elseif movement == "left" then
      canvas_pos_x = canvas_pos_x - 1
      if canvas_pos_x <= 0 then
         movement = "right"
      end
   end
   if scaling == "bigger" then
      canvas_scale_x = canvas_scale_x + 0.001
      if canvas_scale_x >= 1 then
         scaling = "smaller"
      end
   elseif scaling == "smaller" then
      canvas_scale_x = canvas_scale_x - 0.001
      if canvas_scale_x <= 0.75 then
         scaling = "bigger"
      end
   end
end

function spawn_dummyactors(n)
   if n == nil then
      n = 0
   end
   for _ = 1,n do
      game:spawn_dummyactor()
   end
end

---------------------------------------------------------------------------

function love.load()
   local Game = require "classes.game"
   game = Game(400, 400)
   canvas = create_canvas(game.size.x, game.size.y)
   spawn_dummyactors(20)
   local joysticks = love.joystick.getJoysticks()
   joystick = joysticks[1]
end

function love.update()
   if canvas_pos_x == nil then canvas_pos_x = 0 end
   if canvas_scale_x == nil then canvas_scale_x = 1 end
   mx, my = love.mouse.getPosition()
   game.mouse_pos.x = (mx - canvas_pos_x)
   game.mouse_pos.y = (my - 150)
   game:update()
   update_canvas_position()
   if love.keyboard.isDown("u") then
      whoosh = love.audio.newSource("assets/audio/whoosh.wav", "static")
      love.audio.play(whoosh)
      spawn_dummyactors(10) end
   print(joystick)
end

function love.draw()
   game:draw_to_canvas(canvas)
   draw_border_on_canvas(canvas)
   draw_canvas_to_root_window(canvas, canvas_pos_x, 150, 1)
--   game:draw()
   if game.player.shooting.x ~= 0 or game.player.shooting.y ~= 0 then
      love.graphics.print("Player is shooting")      
   end

end

function love.keypressed(key)
   if key == "r" then
      love.load()
   end
end

function love.keyreleased(key)
end
