local Object = require "classes.lib.classic"
local ObjectDirectory = Object:extend()

function ObjectDirectory:call_in_each(funcname, arguments)
   if arguments == nil then arguments = {} end
   for _, item in ipairs(self)  do
      item[funcname](item, unpack(arguments))
   end
end

function ObjectDirectory:register_function_to_call_in_each(funcname_in_self, arguments, funcname_to_call)
   if funcname_to_call == nil then
      funcname_to_call = funcname_in_self
   end
   self[funcname_in_self] = function () self:call_in_each(funcname_to_call, arguments) end
end

function ObjectDirectory:insert_item(item)
   table.insert(self, item)
end

function ObjectDirectory:insert_named_item(item, name)
   -- Creates an accessible variable that links to the list item.
   table.insert(self, item)
   self[name] = self[#self]
end

function ObjectDirectory:get_all_items_recursively()
   -- Returns a list of all items recursively (all that are ObjectDirectories)
   local recursive_list = {}
   for _, item in ipairs(self) do
      if type(item) == "table" then
         if item:is(ObjectDirectory) then
            for _, v in ipairs(item:get_all_items_recursively()) do
               table.insert(recursive_list, v)
            end
         else
            for _, v in ipairs(item) do
               tablet.insert(recursive_list, v)
            end
         end
      else
         table.insert(recursive_list, item)
      end
   end
   return recursive_list
end

function ObjectDirectory:deepcopy()
   -- Cargo-culted from http://lua-users.org/wiki/CopyTable
   local orig = self
   local orig_type = type(orig)
   local copy
   if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in next, orig, nil do
         copy[deepcopy(orig_key)] = deepcopy(orig_value)
      end
      setmetatable(copy, deepcopy(getmetatable(orig)))
   else -- number, string, boolean, etc
      copy = orig
   end
   return copy
end
   
function ObjectDirectory:print()
   for _, item in ipairs(self) do
      print(item)
   end
end

return ObjectDirectory
