local ObjectDirectory = require "classes.lib.objectdirectory"
local nata = require "classes.lib.nata"
local Object = require "classes.lib.classic"
local Game = Object:extend()
local Player = require "classes.actor.player"
local Basic = require "classes.actor.basic"
local Enemy = require "classes.actor.enemy"
local Bullet = require "classes.actor.bullet"

function get_direction(position_from, position_to)
   -- cargo-culted from https://love2d.org/wiki/Tutorial:Fire_Toward_Mouse
   local fromX, fromY = position_from.x, position_from.y
   local toX, toY = position_to.x, position_to.y
   local angle = math.atan2((fromY - toY), (fromX - toX))
   local Dx = math.cos(angle)
   local Dy = math.sin(angle)

   return {x = Dx, y = Dy}
end


function Game:new(size_x, size_y)
   self.size = {x = size_x, y = size_y}
   self.mouse_pos = {x = 0, y = 0}
   local nata_groups = {groups = {
                           bullets = {filter = function(e)
                                         return e.bullet
                                     end},
                           dummies = {filter = function(e)
                                         return e.dummy
                                     end}
                       }}
   self.actors = nata.new(nata_groups)
   self.player = Player(self.size.x / 2,
                        self.size.y / 2,
                        self)
   self.player.shooting = {x = 0, y = 0}
   self.player.moving = {x = 0, y = 0}
   self.actors:queue(self.player)
   self.actors:flush()
   self.score = 0
   self.gameover = false
end

function Game:spawn_dummyactor(velocity)
   if not velocity then local velocity = 1 end
   sx = self.size.x
   sy = self.size.y
   ratio_bounds = {4,12}
   rx = love.math.random(unpack(ratio_bounds))
   ry = love.math.random(unpack(ratio_bounds))
   spawnpoints = {}
   spawnpoints[1] = {x = (sx / rx), y = (sy / ry)}
   spawnpoints[2] = {x = sx - (sx / rx), y = (sy / ry)}
   spawnpoints[3] = {x = sx - (sx / rx), y = sy - (sy / ry)}
   spawnpoints[4] = {x = (sx / rx), y = sy - (sy / ry)}

   next_spawnpoint = spawnpoints[love.math.random(1,4)]
   dummyactor = Enemy(next_spawnpoint.x,
                      next_spawnpoint.y,
                      self,
                      velocity)
   dummyactor.dummy = true
   self.actors:queue(dummyactor)
   self.actors:flush()
end

function Game:onDeath()
   self.gameover = true
end

function Game:_process_actor_collisions()
   local function bullet_hit_dummy(entity)
      for _, d in ipairs(self.actors.groups.dummies.entities) do
         if self.actors.groups.bullets.hasEntity[entity] and
         entity:checkCollision(d) then
            d.dead = true
            self.score = self.score + 1
            return true 
         end
      end
   end

   local function dead(e)
      if e.dead then
         pop = love.audio.newSource("assets/audio/pop.wav", "static")
         love.audio.play(pop)
         return true
      end
   end

   local function bullet_hit_bounds(e)
      if e.bullet then
         local ex = e.position.x
         local ey = e.position.y
         local mx = self.size.x
         local my = self.size.y
         if ex - e.radius <=0 or
            ex + e.radius >= mx or
            ey - e.radius <= 0 or
         ey + e.radius >= my then
            return true
         end
      end
   end

   self.actors:remove(bullet_hit_dummy)
   self.actors:remove(bullet_hit_bounds)
   self.actors:remove(dead)
   
   for _, e in ipairs(self.actors.groups.dummies.entities) do
      if e.dummy and e:checkCollision(self.player) then
         game:onDeath()
      end
   end


   if #self.actors.groups.dummies.entities == 0 then
      if not self.enemy_velocity then self.enemy_velocity = 1
      else
         self.enemy_velocity = self.enemy_velocity + 0.1
      end
      for n = 1,40 do
         self:spawn_dummyactor(self.enemy_velocity)
      end
   end
end

function Game:draw_to_canvas(canvas)
   love.graphics.setCanvas(canvas)
   love.graphics.clear()
   self:draw()
   love.graphics.setCanvas()
end

function Game:draw()
   if self.gameover then
      sx = self.size.x
      sy = self.size.y
      messagepos = {x = sx / 2, y = sy / 2}
      love.graphics.print("Game over", messagepos.x, messagepos.y)
      love.graphics.print("Your score:" , messagepos.x, messagepos.y + 15)
      love.graphics.print(self.score, messagepos.x + 30, messagepos.y + 30)
      love.graphics.print("Press r to restart", messagepos.x, messagepos.y + 45)
   end
   for _, i in ipairs(self.actors.entities) do
      i:draw()
   end
end

function Game:move_enemies_towards_player()
   -- cargo-culted from https://love2d.org/wiki/Tutorial:Fire_Toward_Mouse
   local playerX = self.player.position.x
   local playerY = self.player.position.y
   for _, e in ipairs(self.actors.groups.dummies.entities) do
      local enemyX = e.position.x
      local enemyY = e.position.y

      local D = get_direction({x = playerX, y = playerY},
         {x = enemyX, y = enemyY})

      e.move_direction = {x = D.x * e.velocity, y = D.y * e.velocity}
   end
end
      

function Game:update()
   if not self.gameover then
      self:process_input_realtime()
      for _, i in ipairs(self.actors.entities) do
         i:update()
      end
      self:_process_actor_collisions()
      self:move_enemies_towards_player()
      if self.player.shooting.x ~= 0 or self.player.shooting.y ~= 0 then
         self:shoot(self.player.shooting)
      end
   end
end

function Game:shoot(direction)
   bullet = Bullet(self.player.position.x,
                   self.player.position.y,
                   self,
                   {x = direction.x,
                    y = direction.y})
   self.actors:queue(bullet)
   self.actors:flush()
   return bullet
end

function Game:process_input_realtime()
   local isDown = love.keyboard.isDown
   local moving = self.player.moving
   if isDown("w") then
      self.player.moving.y = -1
   elseif isDown("s") then
      self.player.moving.y = 1
   else
      self.player.moving.y = 0
   end

   if isDown("a") then
      self.player.moving.x = -1
   elseif isDown("d") then
      self.player.moving.x = 1
   else
      self.player.moving.x = 0
   end

   if isDown("up") then
      self.player.shooting.y = -1
   elseif isDown("down") then
      self.player.shooting.y = 1
   else
      self.player.shooting.y = 0
   end

   if isDown("left") then
      self.player.shooting.x = -1
   elseif isDown("right") then
      self.player.shooting.x = 1
   else
      self.player.shooting.x = 0
   end

   mx = self.mouse_pos.x
   my = self.mouse_pos.y
   px = self.player.position.x
   py = self.player.position.y

   if mx and my then
      self.player.shooting = get_direction({x = mx, y = my}, {x = px, y = py})
   end

   if joystick then
      self.player.shooting.x = joystick:getGamepadAxis("rightx")
      self.player.shooting.y = joystick:getGamepadAxis("righty")
      self.player.moving.x = joystick:getGamepadAxis("leftx")
      self.player.moving.y = joystick:getGamepadAxis("lefty")
   end
end   

function Game:process_input_event(key, event)
   -- This is the old way of handling game input, it is now deprecated as it didn't respond well, the input would feel "sticky" and be unresponsive at times.
   --"key" should be the key that has has been pressed or released
   --"event" should be either "keypressed" or "keyreleased"
   if event == "keypressed" then
      if key == "w" then
         self.player.moving.y = -1
      elseif key == "s" then
         self.player.moving.y = 1
      elseif key == "a" then
         self.player.moving.x = -1
      elseif key == "d" then
         self.player.moving.x = 1
      elseif key == "up" then
         self.player.shooting.y = -1
      elseif key == "down" then
         self.player.shooting.y = 1
      elseif key == "left" then
         self.player.shooting.x = -1
      elseif key == "right" then
         self.player.shooting.x = 1
      end
   elseif event == "keyreleased" then
      if key == "w" or key == "s" then
         self.player.moving.y = 0
      elseif key == "a" or key == "d" then
         self.player.moving.x = 0
      elseif key == "up" or key == "down" then
         self.player.shooting.y = 0
      elseif key == "left" or key == "right" then
         self.player.shooting.x = 0
      end
   end
end

return Game
