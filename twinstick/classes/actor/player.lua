local Basic = require "classes.actor.basic"
local Player = Basic:extend()
local player_radius = 10
local moveamount = 3

function Player:new(pos_x, pos_y, game_instance)
   Player.super.new(self, pos_x, pos_y, game_instance)
   self.radius = player_radius
end

function Player:checkCollision(actor)
   Player.super.checkCollision(self, actor)
end

function Player:update()
   local player = self
   local playerpos = self.position
   local playerdiameter = self.radius * 2
   local moving = self.moving
   if self.moving.y > 0 and playerpos.y + playerdiameter <= self.game_instance.size.y then
      player.position.y = player.position.y + moving.y * moveamount
   elseif self.moving.y < 0 and playerpos.y - playerdiameter >= 0 then
      player.position.y = player.position.y + moving.y * moveamount
   end
   if self.moving.x < 0 and playerpos.x - playerdiameter >= 0 then
      player.position.x = player.position.x + moving.x * moveamount
   elseif self.moving.x > 0 and playerpos.x + playerdiameter <= self.game_instance.size.x then
      player.position.x = player.position.x + moving.x * moveamount
   end
end

return Player
