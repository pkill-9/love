local Basic = require "classes.actor.basic"
Bullet = Basic:extend()

function Bullet:new(pos_x, pos_y, game_instance, direction_vector)
   -- direction vector is table of form {x = n, y = n}
   Bullet.super.new(self, pos_x, pos_y, gameinstance)
   self.bullet = true
   self.direction = direction_vector
   self.velocity = 7
   self.radius = 3
end

function Bullet:update()
   self.position.x = self.position.x + self.direction.x * self.velocity
   self.position.y = self.position.y + self.direction.y * self.velocity
end

return Bullet
