require "math"
local Object = require "classes.lib.classic"
local Basic = Object:extend()
local object_radius = 10

function Basic:new(pos_x, pos_y, game_instance)
   self.game_instance = game_instance
   self.position = {x = pos_x, y = pos_y}
   self.radius = object_radius
end

function Basic:update()
   -- Empty function to prevent errors from calling it on a Basic object.
end

function Basic:checkCollision(actor)
   -- Cargo-culted from https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection
   local dx = self.position.x - actor.position.x
   local dy = self.position.y - actor.position.y
   local distance = math.sqrt(dx * dx + dy * dy)   
   
   if (distance < self.radius + actor.radius) then
      return true
   else
      return false
   end
end

function Basic:draw()
   love.graphics.circle("line",
                        self.position.x,
                        self.position.y,
                        self.radius)
end

return Basic
