local Basic = require "classes.actor.basic"
local Enemy = Basic:extend()

function Enemy:new(pos_x, pos_y, game_instance, velocity)
   Enemy.super.new(self, pos_x, pos_y, game_instance)
   self.enemy = true
   self.move_direction = {x = 0, y = 0}
   if velocity then
      self.velocity = velocity
   else
      self.velocity = 1
   end
end

function Enemy:update()
   local direction = self.move_direction
   local velocity = self.velocity
   self.position.x = self.position.x + direction.x * velocity
   self.position.y = self.position.y + direction.y * velocity
end

return Enemy
