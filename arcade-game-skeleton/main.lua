local GameWithCanvas = require "classes.game-with-canvas"
local game = GameWithCanvas(400, 400)

-------------Commands for the main LOVE functions------------
local game_pos_x = 0
local scale_x = 1
local scaling = "smaller"

function update_canvas_move()
      if movement == "right" or movement == nil then
      game_pos_x = game_pos_x + 1
      if game_pos_x >= love.graphics.getWidth() - 400 then
         movement = "left"
      end
   elseif movement == "left" then
      game_pos_x = game_pos_x - 1
      if game_pos_x <= 0 then
         movement = "right"
      end
   end
   if scaling == "bigger" then
      scale_x = scale_x + 0.001
      if scale_x >= 1 then
         scaling = "smaller"
      end
   elseif scaling == "smaller" then
      scale_x = scale_x - 0.001
      if scale_x <= 0.75 then
         scaling = "bigger"
      end
   end
end


function spawn_dummyactors(n)
   if n == nil then
      n = 0
   end
   for _ = 1,n do
      game:spawn_dummyactor()
   end
end

---------------------------------------------------------------------------

function love.load()
   spawn_dummyactors(1)
end

function love.update()
   game:update()
   update_canvas_move()
end

function love.draw()
   love.graphics.draw(game.canvas, game_pos_x, 150, 0, scale_x)
   game:draw()
   if game.actors.player.playershootingup == 1 then
      love.graphics.print("Player is shooting up")
   end
end

function love.keypressed(key)
   game:process_input_event(key, "keypressed")
   if key == "u" then game:spawn_dummyactor() end
end

function love.keyreleased(key)
   game:process_input_event(key, "keyreleased")
end
