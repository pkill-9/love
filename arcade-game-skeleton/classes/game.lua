local Uberlist = require "classes.lib.uberlist"
local Object = require "classes.lib.classic"
local Game = Object:extend()
local Player = require "classes.actor.player"
local Basic = require "classes.actor.basic"

function Game:new(size_x, size_y)
   self.size = {x = size_x, y = size_y}
   self.actors = Uberlist()
   self.player = Player(self.size.x / 2,
                        self.size.y / 2,
                        self)
   self.actors:insert_named_item(self.player, "player")
end

function Game:spawn_dummyactor()
   dummyactor = Basic(self.size.x * love.math.random(),
                      self.size.y * love.math.random(),
                      self)
   self.actors:insert_item(dummyactor)
end

function Game:_process_actor_collisions()
   for i, actor in ipairs(self.actors) do
      if actor:checkCollision(self.actors.player) == 1 and
      actor ~= self.actors.player then
         table.remove(self.actors, i)
         self:spawn_dummyactor()
      end
   end
end

function Game:draw()
   self.actors:call_in_each("draw")
end

function Game:update()
   self.actors:call_in_each("update")
   self:_process_actor_collisions()
end

function Game:process_input_event(key, event)
   --"key" should be the key that has has been pressed or released
   --"event" should be either "keypressed" or "keyreleased"
   if event == "keypressed" then
      if key == "up" then
         self.actors.player.playermovingup = 1
      elseif key == "down" then
         self.actors.player.playermovingdown = 1
      elseif key == "left" then
         self.actors.player.playermovingleft = 1
      elseif key == "right" then
         self.actors.player.playermovingright = 1
      elseif key == "x" then
         self.actors.player.playershootingup = 1
      end
   elseif event == "keyreleased" then
      if key == "up" then
         self.actors.player.playermovingup = 0
      elseif key == "down" then
         self.actors.player.playermovingdown = 0
      elseif key == "left" then
         self.actors.player.playermovingleft = 0
      elseif key == "right" then
         self.actors.player.playermovingright = 0
      elseif key == "x" then
         self.actors.player.playershootingup = 0
      end
   end
end

return Game
