local Game = require "classes.game"

GameWithCanvas = Game:extend()

local function create_canvas(size_x, size_y)
   local canvas = love.graphics.newCanvas(size_x, size_y)
   love.graphics.setCanvas(canvas)
   love.graphics.clear()
   love.graphics.setBlendMode("alpha")
--   love.graphics.setColor(1, 0, 0, 0.5)
   love.graphics.rectangle('fill', 0, 0, 100, 100)
   love.graphics.setCanvas()

   return canvas
end


function GameWithCanvas:new(size_x, size_y)
   GameWithCanvas.super.new(self, size_x, size_y)
   self.canvas = create_canvas(size_x, size_y)
end

function GameWithCanvas:draw()
   love.graphics.setCanvas(self.canvas)
   love.graphics.clear()
   GameWithCanvas.super.draw(self)
   love.graphics.polygon("line",
                         0, 0,
                         self.size.x, 0,
                         self.size.x, self.size.y,
                         0, self.size.y)
   love.graphics.setCanvas()
end

return GameWithCanvas
