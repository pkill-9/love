local Object = require "classes.lib.classic"
local Uberlist = Object:extend()

function Uberlist:call_in_each(funcname)
   for _, item in ipairs(self)  do
      item[funcname](item)
   end
end

function Uberlist:insert_item(item)
   table.insert(self, item)
end

function Uberlist:insert_named_item(item, name)
   -- Creates an accessible variable that links to the list item.
   table.insert(self, item)
   self[name] = self[#self]
end

function Uberlist:print()
   for _, item in ipairs(self) do
      print(item)
   end
end

return Uberlist
