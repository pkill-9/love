local Basic = require "classes.actor.basic"
local Player = Basic:extend()
local player_radius = 10
local player_moveamount = 5

function Player:new(pos_x, pos_y, game_instance)
   Player.super.new(self, pos_x, pos_y, game_instance)
   self.radius = player_radius
end

function Player:checkCollision(actor)
   Player.super.checkCollision(self, actor)
end

function Player:update()
   local player = self
   local playerpos = self.position
   local playerdiameter = self.radius * 2
   if self.playermovingdown == 1 and playerpos.y + playerdiameter <= self.game_instance.size.y then
      player.position.y = player.position.y + player_moveamount
   elseif self.playermovingup == 1 and playerpos.y - playerdiameter >= 0 then
      player.position.y = player.position.y - player_moveamount
   elseif self.playermovingleft == 1 and playerpos.x - playerdiameter >= 0 then
      player.position.x = player.position.x - player_moveamount
   elseif self.playermovingright == 1 and playerpos.x + playerdiameter <= self.game_instance.size.x then
      player.position.x = player.position.x + player_moveamount
   end
end

return Player
